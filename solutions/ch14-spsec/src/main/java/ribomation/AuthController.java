package ribomation;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/auth")
public class AuthController {

    @GetMapping("login")
    public String login(Model data, @RequestParam(required = false) Boolean failure) {
        if (failure != null && failure.booleanValue()) {
            data.addAttribute("message", "Invalid username/password");
        }
        return "login";
    }
}
