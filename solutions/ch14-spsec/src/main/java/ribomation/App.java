package ribomation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@SpringBootApplication
@EnableWebSecurity
public class App implements WebMvcConfigurer {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("redirect:/home");
        registry.addViewController("/home").setViewName("home");
        registry.addViewController("/protected").setViewName("protected");
    }

    @Bean
    public List<UserDetails> users(PasswordEncoder pwdEnc) {
        return List.of(
                User.withUsername("anna")
                        .password("secret")
                        .passwordEncoder(pwdEnc::encode)
                        .roles("USER")
                        .build(),

                User.withUsername("berit")
                        .password("secret")
                        .passwordEncoder(pwdEnc::encode)
                        .roles("USER")
                        .build()
        );
    }

    @Bean
    public UserDetailsService userDetailsService(List<UserDetails> users) {
        System.out.printf("[usr] populating %s%n", users);
        return new InMemoryUserDetailsManager(users) {
            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                var user = super.loadUserByUsername(username);
                System.out.printf("[usr] %s --> %s%n", username, user);
                return user;
            }
        };
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder() {
            @Override
            public String encode(CharSequence rawPassword) {
                var encodedPassword = super.encode(rawPassword);
                System.out.printf("[pwd] %s --> %s%n", rawPassword, encodedPassword);
                return encodedPassword;
            }

            @Override
            public boolean matches(CharSequence rawPassword, String encodedPassword) {
                var ok = super.matches(rawPassword, encodedPassword);
                System.out.printf("[pwd] %s ~ %s --> %b%n", rawPassword, encodedPassword, ok);
                return ok;
            }
        };
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(req -> {
            req.requestMatchers("/", "/home", "/style*", "/favicon.ico",
                    "/auth/*", "/error"
            ).permitAll();
            req.requestMatchers("/protected").authenticated();
        });

        http.formLogin(req -> {
            req.loginPage("/auth/login");
            req.loginProcessingUrl("/auth/login");
            req.defaultSuccessUrl("/protected");
            req.failureUrl("/auth/login?failure=true");
        });

        http.logout(req -> {
            req.logoutUrl("/auth/logout");
            req.logoutSuccessUrl("/home");
            req.invalidateHttpSession(true);
            req.clearAuthentication(true);
        });

        http.sessionManagement(req -> {
            req.sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED);
            req.maximumSessions(1);
            req.invalidSessionUrl("/home");
        });

        http.csrf(csrf -> {
            csrf.disable();
        });

        return http.build();
    }

}
