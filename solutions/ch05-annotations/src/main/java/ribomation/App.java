package ribomation;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ribomation.domain.PersonCsvMapper;
import ribomation.domain.PersonJsonMapper;
import ribomation.domain.PersonLoader;
import ribomation.domain.PersonRepo;
import ribomation.jdbc.H2DataSourceBuilder;
import ribomation.jdbc.JdbcClassicPersonRepo;
import ribomation.jdbc.PersonJdbcMapper;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;

@Configuration
@ComponentScan(basePackages = {"ribomation.domain", "ribomation.jdbc"})
public class App {
    public static void main(String[] args) throws Exception {
        var app = new App();
        //app.run();
//        app.runXML();
//        app.runJavaConfig();
        app.runAnnotations();
    }

     void runAnnotations() throws IOException {
         var ctx = new AnnotationConfigApplicationContext(App.class);
//         var csvResource = ctx.getResource("classpath:/persons.csv").getInputStream();
//         var loader = ctx.getBean(PersonLoader.class);
//         var jsonMapper = ctx.getBean(PersonJsonMapper.class);
//         var dao = ctx.getBean(PersonRepo.class);

         var useCase = (UseCase) ctx.getBean("useCase");
         useCase.run();
    }

    @Bean("useCase")
    UseCase useCase(InputStream csvResource, PersonLoader loader, PersonJsonMapper jsonMapper, PersonRepo dao) {
        return new UseCase(csvResource, loader, dao, jsonMapper);
    }

    @Bean
    InputStream csvResource() {
        return getClass().getResourceAsStream("/persons.csv");
    }

/*
    void runJavaConfig() throws IOException {
        var ctx = new AnnotationConfigApplicationContext(App.class);

        var csvResource = ctx.getResource("classpath:/persons.csv").getInputStream();
        var loader = ctx.getBean("personLoader", PersonLoader.class);
        var jsonMapper = ctx.getBean("jsonMapper", PersonJsonMapper.class);
        var dao = ctx.getBean("personRepo", PersonRepo.class);

        var useCase = new UseCase(csvResource, loader, dao, jsonMapper);
        useCase.run();
    }

    @Bean
    PersonRepo personRepo(PersonJdbcMapper mapper, Connection ds) {
        var repo = new JdbcClassicPersonRepo();
        repo.setMapper(mapper);
        repo.setDataSource(ds);
        repo.createTable();
        return repo;
    }

    @Bean
    PersonJdbcMapper jdbcMapper() {
        return new PersonJdbcMapper();
    }

    @Bean
    Connection datasource() {
        return new H2DataSourceBuilder().openServer();
    }

    @Bean
    PersonJsonMapper jsonMapper() {
        return new PersonJsonMapper();
    }

    @Bean
    PersonLoader personLoader(PersonCsvMapper csvMapper) {
        var loader = new PersonLoader();
        loader.setMapper(csvMapper);
        return loader;
    }

    @Bean
    PersonCsvMapper csvMapper() {
        return new PersonCsvMapper(";");
    }
*/

/*
    void runXML() throws IOException {
        var ctx = new ClassPathXmlApplicationContext("/beans.xml");

        var csvResource = ctx.getResource("classpath:/persons.csv").getInputStream();
        var loader = ctx.getBean("personLoader", PersonLoader.class);
        var jsonMapper = ctx.getBean("jsonMapper", PersonJsonMapper.class);
        var dao = ctx.getBean("personRepo", JdbcClassicPersonRepo.class);

        var useCase = new UseCase(csvResource, loader, dao, jsonMapper);
        useCase.run();
    }
*/

/*
    void run() {
        var csvResource = getCsvResource();
        var loader      = getPersonLoader();
        var dao         = getPersonRepo();
        var jsonMapper  = getJsonMapper();
        var useCase     = new UseCase(csvResource, loader, dao, jsonMapper);
        useCase.run();
    }
*/

/*
    PersonRepo getPersonRepo() {
        var repo = new JdbcClassicPersonRepo();
        repo.setDataSource(getDataSource());
        repo.setMapper(getJdbcMapper());
        repo.createTable();
        return repo;
    }

    PersonLoader getPersonLoader() {
        var loader = new PersonLoader();
        loader.setMapper(getCsvMapper());
        return loader;
    }

    InputStream getCsvResource() {
        return getClass().getResourceAsStream("/persons.csv");
    }
    Connection getDataSource() {
        return new H2DataSourceBuilder().openServer();
    }
    PersonCsvMapper getCsvMapper() {
        return new PersonCsvMapper(";");
    }
    PersonJdbcMapper getJdbcMapper() {
        return new PersonJdbcMapper();
    }
    PersonJsonMapper getJsonMapper() {
        return new PersonJsonMapper();
    }
*/


}

