package ribomation.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ribomation.domain.PersonDAO;

@Controller
@RequestMapping("/")
public class PersonViewController {
    @Autowired
    private PersonDAO dao;

    @GetMapping("show/{id}")
    public String show(@PathVariable int id, Model data) {
        if (dao.existsById(id)) {
            data.addAttribute("id", id);
            return "show";
        }
        return "redirect:/list";
    }

    @GetMapping("edit/{id}")
    public String edit(@PathVariable int id, Model data) {
        if (dao.existsById(id)) {
            data.addAttribute("id", id);
            return "edit";
        }
        return "redirect:/list";
    }

    @GetMapping("create")
    public String create(Model data) {
        data.addAttribute("id", -1);
        return "edit";
    }

}
