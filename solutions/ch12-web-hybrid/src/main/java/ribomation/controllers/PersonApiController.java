package ribomation.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ribomation.domain.Person;
import ribomation.domain.PersonDAO;

import java.util.List;

@RestController
@RequestMapping("/api/persons")
public class PersonApiController {
    @Autowired
    private PersonDAO dao;

    @GetMapping
    public List<Person> list() {
        return dao.findAll();
    }

    @GetMapping("/{id}")
    public Person one(@PathVariable int id) {
        if (id == -1) {
            return dao.create();
        } else {
            return dao.findById(id).orElseThrow();
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        dao.delete(id);
    }

    @PutMapping("/{id}")
    public Person update(@PathVariable int id, @RequestBody Person delta) {
        dao.update(id, delta);
        return dao.findById(id).orElseThrow();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Person create(@RequestBody Person delta) {
        var p = dao.create();
        dao.insert(p);
        dao.update(p.getId(), delta);
        return dao.findById(p.getId()).orElseThrow();
    }


}
