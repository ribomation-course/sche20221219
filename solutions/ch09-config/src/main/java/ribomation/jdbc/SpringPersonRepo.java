package ribomation.jdbc;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ribomation.domain.Person;
import ribomation.domain.PersonRepo;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

@Repository
public class SpringPersonRepo implements PersonRepo, InitializingBean {
    private final JdbcTemplate jdbc;
    private final RowMapper<Person> mapper;

    public SpringPersonRepo(DataSource ds, RowMapper<Person> mapper) {
        this.jdbc = new JdbcTemplate(ds);
        this.mapper = mapper;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        createTable();
    }

    private void createTable() {
        final String tableSQL = """
                DROP TABLE IF EXISTS persons;
                CREATE TABLE persons (
                    id          int primary key auto_increment,
                    name        varchar(64),
                    age         int,
                    gender      varchar(6),
                    postCode    int
                );
                """;
        jdbc.execute(tableSQL);
    }

    @Override
    public int countAll() {
        String sql = "SELECT count(*) FROM " + TABLE_NAME;
        return jdbc.queryForObject(sql, Integer.class);
    }

    @Override
    public List<Person> findAll() {
        String sql = "SELECT * FROM " + TABLE_NAME;
        return jdbc.query(sql, mapper);
    }

    @Override
    public Optional<Person> findById(int id) {
        String sql = "SELECT * FROM " + TABLE_NAME + " WHERE id = ?";
        try {
            return Optional.of(jdbc.queryForObject(sql, mapper, id));
        } catch (DataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<Person> findByAgeBetweenAndPostCodeLessThanAndFemale(int ageLower, int ageUpper, int postCodeUpper) {
        String sql = "SELECT * FROM " + TABLE_NAME + " WHERE age BETWEEN ? AND ? AND postCode <= ? AND gender = 'Female'";
        return jdbc.query(sql, mapper, ageLower, ageUpper, postCodeUpper);
    }

    @Override
    public int insert(Person person) {
        return 0;
    }

    @Override
    public void insert(List<Person> persons) {
        String sql = "insert into " + TABLE_NAME + " (name,age,gender,postcode) values (?,?,?,?)";
        ParameterizedPreparedStatementSetter<Person> preparator= (ps, p) -> {
            ps.setString(1, p.getName());
            ps.setInt(2, p.getAge());
            ps.setString(3, p.isFemale() ? "Female" : "Male");
            ps.setInt(4, p.getPostCode());
        };
        jdbc.batchUpdate(sql, persons, 100, preparator);
    }

    @Override
    public void update(int id, Person p) {
        String sql = "UPDATE " + TABLE_NAME + " SET name=?, age=?, gender=?, postCode=? WHERE id=?";
        jdbc.update(sql, p.getName(), p.getAge(), p.isFemale() ? "Female" : "Male", p.getPostCode(), id);
    }

    @Override
    public void delete(int id) {
        String sql = "DELETE FROM " + TABLE_NAME + " WHERE id=?";
        jdbc.update(sql, id);
    }
}
