package ribomation;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "ribomation.app")
@Data
@NoArgsConstructor
public class ApplicationConfig {
    String name;
    String version;
}
