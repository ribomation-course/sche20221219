#!/usr/bin/env bash
set -eux

DIR=./db-files
JAR=$DIR/h2.jar
SRV=org.h2.tools.Server

java -cp $JAR $SRV -tcp -baseDir $DIR -ifNotExists
