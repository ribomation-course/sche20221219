package ribomation.ch07boot;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Ch07BootApplication {
    public static void main(String[] args) {
        SpringApplication.run(Ch07BootApplication.class, args);
    }

    @Bean
    public CommandLineRunner hello() {
        return args -> {
            System.out.println("*** Hello from a Spring Boot app ***");
        };
    }
}
