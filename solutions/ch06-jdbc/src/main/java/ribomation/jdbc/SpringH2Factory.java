package ribomation.jdbc;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Service;

@Service
public class SpringH2Factory extends DriverManagerDataSource {
    static final String url = "jdbc:h2:tcp://localhost:9092/persons";
    static final String usr = "sa";
    static final String pwd = "";

    public SpringH2Factory() {
        super(url, usr, pwd);
    }
}
