package ribomation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import ribomation.domain.PersonJsonMapper;
import ribomation.domain.PersonLoader;
import ribomation.domain.PersonRepo;

import java.io.IOException;

@Configuration
@ComponentScan(basePackages = {"ribomation.domain", "ribomation.jdbc"})
public class App {
    public static void main(String[] args) throws Exception {
        var app = new App();
        app.run();
    }

    void run() {
        var ctx = new AnnotationConfigApplicationContext(App.class);
        var useCase = (UseCase) ctx.getBean("useCase");
        useCase.run();
    }

    @Bean("useCase")
    UseCase useCase(Resource csv, PersonLoader loader, PersonJsonMapper jsonMapper, PersonRepo dao) throws IOException {
        return new UseCase(csv.getInputStream(), loader, dao, jsonMapper);
    }

    @Bean
    Resource csv(ApplicationContext ctx) {
        return ctx.getResource("/persons.csv");
    }

}

