package ribomation.domain;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "create")
public class Person {
    //name;age;gender;postCode

    @Id @GeneratedValue
    private int id;

    private String name;
    private int age;
    private boolean female;
    private int postCode;

}
