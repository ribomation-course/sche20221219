package ribomation;

import org.springframework.stereotype.Service;
import ribomation.domain.PersonJsonMapper;
import ribomation.domain.PersonLoader;
import ribomation.domain.PersonRepo;

import java.io.InputStream;

@Service
public class UseCase {
    InputStream csvResource;
    PersonLoader loader;
    PersonRepo dao;
    PersonJsonMapper jsonMapper;

    public UseCase(InputStream csvResource, PersonLoader loader, PersonRepo dao, PersonJsonMapper jsonMapper) {
        this.csvResource = csvResource;
        this.loader = loader;
        this.dao = dao;
        this.jsonMapper = jsonMapper;
    }

    public void run() {
        dao.saveAll(loader.loadAll(csvResource));
        System.out.printf("inserted %d persons%n", dao.count());

        var lst = dao.findByAgeBetweenAndPostCodeLessThanAndFemale(30, 40, 12_500, true);
        System.out.printf("found %d persons%n", lst.size());
        System.out.printf("JSON: %s%n", jsonMapper.toJson(lst));

        dao.deleteAll(lst);
        System.out.printf("removed %d persons%n", lst.size());
        System.out.printf("reduced size to %d persons%n", dao.count());

        var obj = dao.findById(1).orElseThrow();
        System.out.printf("found %s%n", obj);
        //new Person("Per Silja", 42, false, 25_000)
        obj.setName("Per Silja");
        obj.setAge(42);
        obj.setFemale(false);
        obj.setPostCode(25_000);
        dao.save(obj);
        System.out.printf("modified %s%n", dao.findById(1).orElseThrow());
    }
}


