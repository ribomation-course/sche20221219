package ribomation.ch08lombok;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(staticName = "create")
public class Person {
    //name;age;gender;postCode
    private int id;
    private String name;
    private int age;
    private boolean female;
    private int postCode;

}
