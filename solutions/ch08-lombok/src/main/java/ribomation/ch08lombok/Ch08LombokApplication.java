package ribomation.ch08lombok;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;

import java.util.List;

@SpringBootApplication
public class Ch08LombokApplication {
    public static void main(String[] args) {
        SpringApplication.run(Ch08LombokApplication.class, args);
    }

    @Bean
    public CommandLineRunner load(PersonLoader loader, ApplicationContext ctx) {
        return args -> {
            var resource = ctx.getResource("/persons.csv");
            var list = loader.loadAll(resource.getInputStream());
            list.stream().limit(10).forEach(System.out::println);
        };
    }

}
