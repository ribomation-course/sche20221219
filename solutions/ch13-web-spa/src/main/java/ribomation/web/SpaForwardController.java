package ribomation.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SpaForwardController {

    @GetMapping({"/list", "/show/*", "/edit/*"})
    public String fwd2idx() {
        return "forward:/index.html";
    }

}
