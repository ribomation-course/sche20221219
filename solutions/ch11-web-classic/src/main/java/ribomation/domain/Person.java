package ribomation.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "create")
public class Person {
	private Integer id;
    private String name;
	private Integer age;
    private String avatar;
    private String company;
    private String email;

    public static Person empty() {
        return new Person();
    }
}
