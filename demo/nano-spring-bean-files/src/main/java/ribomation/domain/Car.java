package ribomation.domain;

import java.util.Objects;

public class Car {
    private final String model;

    private final Engine engine;

    public Car(String model, Engine engine) {
        this.model = model;
        this.engine = engine;
    }

    @Override
    public String toString() {
        return String.format("Car{%s, %s}", model, engine);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(model, car.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(model);
    }

    public String getModel() {
        return model;
    }

    public Engine getEngine() {
        return engine;
    }
}
