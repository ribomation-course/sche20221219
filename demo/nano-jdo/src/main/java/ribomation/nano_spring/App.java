package ribomation.nano_spring;

import ribomation.nano_spring.api.impl.*;
import ribomation.nano_spring.domain.Invoice;
import ribomation.nano_spring.domain.InvoiceDAO;
import ribomation.nano_spring.domain.Person;
import ribomation.nano_spring.domain.PersonDAO;
import ribomation.nano_spring.jdbc.H2;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.System.out;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.runNanoJdo();
        app.runNanoJdo2();
    }

    void runNanoJdo() {
        out.println("--- Invoice Objects ---");
        var h2      = new H2();
        var factory = new DaoFactory<>(InvoiceDAO.class, Invoice.class);

        try (var dao = factory.create(h2.open(H2.H2Types.memory, "invoice_db"))) {
            dao.createTable();
            dao.withConnection(c -> out.printf("tbl: %s%n", h2.tableNames(c)));

            Stream.generate(this::mkInvoice).limit(5).forEach(dao::insert);
            out.printf("inserted: %d rows%n", dao.count());

            var all = dao.all();
            all.forEach(out::println);

            var first = all.get(0);
            out.printf("one    : %s%n", dao.findById(first.getId()).orElseThrow());
            out.printf("(a) few: %s%n", dao.findAllByCustomer(first.getCustomer()));
            out.printf("(b) few: %s%n", dao.findAllByAmount(first.getAmount()));

            dao.remove(first);
            out.printf("after remove: %d rows%n", dao.count());
        }
    }

    void runNanoJdo2() {
        out.println("--- Person Objects ---");
        var h2      = new H2();
        var factory = new DaoFactory<>(PersonDAO.class, Person.class);
        try (var dao = factory.create(h2.open(H2.H2Types.memory, "person_db"))) {
            dao.createTable();
            dao.withConnection(c -> out.printf("tbl: %s%n", h2.tableNames(c)));

            Stream.generate(this::mkPerson).limit(100).forEach(dao::insert);
            out.printf("inserted: %d rows%n", dao.count());

            out.printf("age 25: %s%n", dao.findAllByAge(25));
        }
    }

    Invoice mkInvoice() {
        return new Invoice(mkDate.get(), mkName.get(), mkAmount.get());
    }

    Person mkPerson() {
        return new Person(mkName.get(), mkAge.get());
    }

    static Random r = new Random();
    static List<String> names = List.of(
            "Anna Conda", "Per Silja", "Justin Time", "Inge Vidare",
            "Ana Gram", "K A Ramell", "Sham Poo", "Pär Onmos", "Åke R Vidare"
    );
    static Supplier<String>  mkName   = () -> names.get(r.nextInt(names.size()));
    static Supplier<Integer> mkAmount = () -> (int) (1000 + 250 * r.nextGaussian());
    static Supplier<Integer> mkAge    = () -> (int) (20 + r.nextInt(10));
    static Supplier<Date>    mkDate   = () -> Date.from(
            LocalDateTime.now().minusDays(r.nextInt(90)).toInstant(ZoneOffset.of("+1"))
    );
}
