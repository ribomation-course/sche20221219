package ribomation.nano_spring.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ClasspathLoaderTest {
    ClasspathLoader loader;
    BeanRepositoryImpl repo;
    String pkgName = "ribomation.nano_spring.impl.tst";

    @BeforeEach
    void setUp() {
        repo = new BeanRepositoryImpl();
        loader = new ClasspathLoader(repo);
    }

    URL getUrl(String pkgName) {
        var clsLoader = Thread.currentThread().getContextClassLoader();
        return clsLoader.getResource(pkgName.replace('.', '/'));
    }

    @Test
    void tst_loadClassNames() throws URISyntaxException {
        var classNames = loader.loadClassNames(pkgName);
        // System.out.printf("names: %s%n", classNames);

        assertEquals(3, classNames.size());
        var last = classNames.get(2);
        assertEquals("C", last.substring(last.length() - 1));
    }

    @Test
    void tst_getBeanClassNames() throws URISyntaxException {
        var classes = loader.loadClassNames(pkgName);
        var beanClassNames = loader.getBeanClassNames(pkgName, classes);
        // System.out.printf("beans: %s%n", beanClassNames);

        assertEquals(2, beanClassNames.size());
    }

}
