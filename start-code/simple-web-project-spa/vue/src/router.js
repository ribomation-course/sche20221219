import {createRouter, createWebHistory} from 'vue-router'
import NotFoundPage from './pages/not-found.vue'
import ListPage from './pages/list.vue'
import ShowPage from './pages/show.vue'
import EditPage from './pages/edit.vue'

const routes = [
    {path: '/list', name: 'list', component: ListPage},
    {path: '/show/:id', name: 'show', props: true, component: ShowPage},
    {path: '/edit/:id', name: 'edit', props: true, component: EditPage},

    {path: '/', redirect: {name: 'list'}},
    {path: '/:pathMatch(.*)*', component: NotFoundPage}
]

const router = createRouter({
    history: createWebHistory(),
    routes,
})
export default router;
