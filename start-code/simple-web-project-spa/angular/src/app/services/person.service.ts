import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Person} from "../domain/person";

const url = 'http://localhost:8080/api/persons'


@Injectable({
    providedIn: 'root'
})
export class PersonService {
    constructor(private httpClient: HttpClient) {}

    findAll(): Observable<Person[]> {
        return this.httpClient.get<Person[]>(url);
    }

    findById(id: number): Observable<Person> {
        return this.httpClient.get<Person>(`${url}/${id}`);
    }

    insert(person: Person): Observable<Person> {
        return this.httpClient.post<Person>(url, person);
    }

    update(id :number, person: Person): Observable<Person> {
        return this.httpClient.put<Person>(`${url}/${id}`, person);
    }

    remove(id :number): Observable<void> {
        return this.httpClient.delete<void>(`${url}/${id}`);
    }
}
