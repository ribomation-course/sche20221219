import {Component, OnInit} from '@angular/core';
import {PersonService} from "../../services/person.service";
import {Person} from "../../domain/person";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-show',
    templateUrl: './show.page.html',
    styleUrls: ['./show.page.css']
})
export class ShowPage implements OnInit {
    person: Person | undefined;

    constructor(
        private personSvc: PersonService,
        private route: ActivatedRoute,
        private router: Router
    ) {
    }

    ngOnInit() {
        const id = this.route.snapshot.paramMap.get('id');
        this.loadItem(id);
    }

    async cancel() {
        await this.router.navigate(['/list'])
    }

    loadItem(id: number | string | null) {
        if (!!id) {
            id = Number(id);
            this.personSvc
                .findById(id)
                .subscribe(obj => this.person = obj);
        }
    }

    removeItem(id: number | undefined) {
        if (!!id) {
            this.personSvc
                .remove(id)
                .subscribe(() => this.cancel());
        }
    }

}
