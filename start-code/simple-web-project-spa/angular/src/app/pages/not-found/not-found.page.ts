import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-not-found',
    template: `
        <h1 >Page Not Found</h1>
        <div>
            <img [src]="image" alt="not found" class="img-fluid">
        </div>
    `,
    styles: [`img {width: 20em;}`]
})
export class NotFoundPage {
    image = 'assets/not-found.png'
}
